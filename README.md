* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

*Add info on ASP.NET helper tags, Fix Middleware. Add information on Razor syntax.*

#**MVC**#

**What is MVC?**

+ Model-View-Controller(MVC) is a pattern that breaks up an application into three components: model, view, and controller. 

*Requirements for Using MVC*

+ Visual Studio 2015 with:
    1. Microsoft Web Developer Tools
    2. Powershell Tools
    3. C#/.NET
    4. WebSocket4Net
    5. Common Tools and Software Development Kits

**Controllers**

*What is a Controller?*

+ The controller is an interface between the model and the view. It processes business logic and requests, handles data for the model, and works with the views to create the visual output.
+ MVC invokes controllers, and the actions within them, based on the incoming URL. 
+ Controllers pass information to and return views to be displayed to the user. 

*How Do You Use Them?*

+ Open or create an MVC application and right click the Controller folder. 
+ Open the Add menu and select Controller.


![383Img1.png](https://bitbucket.org/repo/g8pdKG/images/3504046241-383Img1.png)


+ Name your controller.	


![383Img2.png](https://bitbucket.org/repo/g8pdKG/images/2603596130-383Img2.png)


![383Img3.png](https://bitbucket.org/repo/g8pdKG/images/3656466412-383Img3.png)


![383Img4.png](https://bitbucket.org/repo/g8pdKG/images/2634139423-383Img4.png)


**Actions**

+ Are methods inside the controller, and contain the application's logic.
+ A single controller can contain as many actions as desired, and actions are typically single purpose. 
+ By default, the mapping for MVC is /Controller/Action/Parameter. An example URL with no parameters would be http://www.example.com/Home/LandingPage

**Models**

*What is a Model?*

+ The model represents all of the data related logic that users interact with, such as data being moved between the view and controller.
+ Models allow for code-first development, where databases are generated from classes in your project.

*Using a Model*

+ In your MVC application, right click the Models folder.
+ Open the Add menu, and select Class.


![383Img9.png](https://bitbucket.org/repo/g8pdKG/images/4218994152-383Img9.png)


+ Name your new model.


![383Img10.png](https://bitbucket.org/repo/g8pdKG/images/1760991167-383Img10.png)


**Modelbinding**

+ Model Binding takes values from an HTML page and maps them to the appropriate model, allowing the developer to skip HTML-Model mapping.
+ The model binder can pull data from HTML forms, POST variables, query strings, and other values.

**Views**

+ The view is the user interface of the application. This includes buttons, text input fields, etc.
+ Browser requests are mapped to controllers and actions, which can then return a view. 
+ Typically, views will contain a combination of HTML and scripts written in a .net language such as C#.


![383Img5.png](https://bitbucket.org/repo/g8pdKG/images/3973586374-383Img5.png)


![383Img6.png](https://bitbucket.org/repo/g8pdKG/images/1257626555-383Img6.png)


![383Img7.png](https://bitbucket.org/repo/g8pdKG/images/3499617137-383Img7.png)


![383Img8.png](https://bitbucket.org/repo/g8pdKG/images/1644549752-383Img8.png)

###WebAPI###

*What is the ASP.NET Web API?*

+ Web API is a framework for building HTTP services for a variety of users.
+ Web API is particularly helpful for building RESTful applications in .NET
+ Web API is very similar to MVC, containing many similar features such as routing, controllers, model binders, and dependency injection.

*Why use Web API?*

+ Web API allows you to reach a broader audience by catering to a wide variety of devices.
+ Web API uses all of the features of HTTP without requiring any extra configuration for different devices.
+ Web API is light weight, making it very useful for people trying to reach mobile devices.


###ASP.NET Core vs ASP.NET###

+ ASP.NET core is an open-source and cross-platform framework used for creating internet connected applications and offers a great framework for those that are deployed to the cloud or run on-premises.

*Reasons to use .NET Core include:*

+ Its API surface can be minimized to the scopes of several different microservices. 
+ It is not based on System.Web.dll allowing it to be a much more modular and flexible framework.
+ It is based entirely on NuGet packages.
+ It has a unified story for building web UI and web APIs.

###Middleware###

Middleware is a software that acts as a bridge between already existing programs. Usually these include complex enterprise applications and Web services. 

ASP.NET Core’s processing pipeline is based on middleware. During the making of the application the programmer is able to customize what pieces of middleware will be used and in what order they should execute. 


+ Applications are leaner because only the features that are required are installed.
+ Since middleware is assembled into a pipeline that handles requests and responses each of the middleware pieces has the option of either creating a response or calling on to the next piece. 
+ It allows ASP.NET Core to use less memory.





###HTTPApplication LifeCycle###


![383 HTTPApplication Lifecycle.png](https://bitbucket.org/repo/g8pdKG/images/3021926134-383%20HTTPApplication%20Lifecycle.png)



* User requests an application resource from the web server.
* The authentication request confirms that the request is authenticated before processing the attached module or handle. 
* The Authorization request indicates that ASP.NET has authorized the current request and the user has also been authorized.
* The caching modules server requests from the cache and calls any end request handlers. 
* Used by the ASP.NET infrastructure to determine the request handler for the current request based on the file-name extension, then ASP.NET maps the current request to the appropriate HTTP handler
* In order for ASP.NET to acquire and obtain the current state that is associated with the current request, there must be a valid session ID. 
* The handle generates output; this is the only event that isn’t exposed by the HTTPApplication class.
* The request has been logged. The log request event is raised even if an error occurs
* Last event of execution when ASP.NET responds to a request
* ASP.NET sends the content to the client



###OWIN###

An open standard for an interface between web applications and .NET web servers. 

* It is a community owned, open source project.
* It’s meant as a way to decouple server and application and promote simple modules for.NET web development
* If implemented correctly, ASP.NET applications will run on user’s servers